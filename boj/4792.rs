// 레드 블루 스패닝 트리
// 시간 제한 	메모리 제한 	제출 	정답 	맞은 사람 	정답 비율
// 3 초 	128 MB 	637 	132 	99 	21.663%
// 문제

// 무방향, 무가중치, 연결 그래프가 주어진다. 그래프의 각 간선은 빨간색 또는 파란색으로 색칠되어져 있다. 이 그래프의 스패닝 트리 중 파란색 간선이 정확히 k개인 것이 있는지 없는지 알아내는 프로그램을 작성하시오.
// 입력

// 입력은 여러 개의 테스트 케이스로 이루어져 있다.

// 각 테스트 케이스의 첫 줄에는 세 정수 n, m, k가 주어진다. n은 그래프의 정점의 개수 (2 ≤ n ≤ 1,000)이고, m은 간선의 개수, k는 문제에 설명되어 있는 파란색 간선의 개수 (0 ≤ k < n) 이다.

// 다음 m개 줄에는 간선의 정보가 주어지며, 각 정보는 세 정수 c, f, t로 이루어져 있다. c는 간선의 색상을 나타내며, 빨간색인 경우에는 R, 파란색인 경우에는 B이다. f와 t는 정수로 간선이 연결하는 두 정점을 나타낸다. (1 ≤ f,t ≤ n, t ≠ t) 두 정점을 연결하는 간선은 최대 한 개이다.

// 입력의 마지막 줄에는 0이 세 개 주어진다.
// 출력

// 각 테스트 케이스마다 파란색 간선이 정확하게 k개인 스패닝 트리를 만들 수 있으면 1, 없으면 0을 출력한다.

use std::io;

#[derive(Default)]
struct ColoredGraph {
  nodes : Vec<Node>,
  red_edges: Vec<Edge>,
  blue_edges : Vec<Edge>
}

#[derive(Clone, Default)]
struct Node {
  parent : Option<usize>,
  rank : i32
}

struct Edge (usize,usize);
enum Color{
  Red,
  Blue
}

trait UnionFind {
  fn find(&mut self, usize) -> usize ;
  fn union(&mut self, usize, usize) -> bool;
}
impl UnionFind for Vec<Node> {
  fn find(&mut self, node_id:usize) -> usize {
      match self[node_id].parent {
          Some(parent_id) => {
              let new_parent = self.find(parent_id);
              self[node_id].parent = Some(new_parent);
              new_parent
          },
          None => node_id
      }
  }
  fn union(&mut self, node1_id: usize, node2_id: usize) -> bool {
      let parent1 = self.find(node1_id);
      let parent2 = self.find(node2_id);
      if self[parent1].rank < self[parent2].rank {
          self[parent1].parent = Some(parent2);
      } else if self[parent1].rank > self[parent2].rank {
          self[parent2].parent = Some(parent1);
      } else if parent1 != parent2 {
          self[parent2].parent = Some(parent1);
          self[parent1].rank +=1;
      }
      parent1 != parent2
  }
}

impl ColoredGraph {
  fn kruskal(&mut self, color : Color) -> usize {
    self.nodes = vec![Default::default(); self.nodes.len()];
    let iters = match color {
        Color::Red => (&self.red_edges, &self.blue_edges),
        Color::Blue => (&self.blue_edges, &self.red_edges)
    };
    let mut colorcount = 0;
    for edge in iters.0 {
        if self.nodes.union(edge.0,edge.1) {
            colorcount+=1;
        }
    }
    for edge in iters.1 {
        self.nodes.union(edge.0, edge.1);
    }
    colorcount
  }
}

fn read_graph_info () -> (usize,usize,usize) {
  let mut s = String::new();    
  io::stdin().read_line(&mut s).unwrap();
  let arg : Vec<usize> = s.as_mut_str().split_whitespace().map(|s| s.parse().unwrap()).collect();
  (arg[0],arg[1],arg[2])
}

fn read_edges_info (nodes: usize, edges: usize) -> ColoredGraph {
  let mut graph : ColoredGraph = Default::default();
  graph.nodes = vec![Default::default(); nodes+1];
  for _i in 0..edges {
    let mut s = String::new();
    io::stdin().read_line(&mut s).unwrap();
    let arg : Vec<&str> = s.as_mut_str().split_whitespace().collect();
    let new_edge :Edge = Edge( arg[1].parse().unwrap() , arg[2].parse().unwrap() );
    match arg[0] {
      "B" => {graph.blue_edges.push(new_edge);}
      "R" => {graph.red_edges.push(new_edge);}
      _ => {break;}
    };
  }
  graph
}

fn main () {
  loop {
    match read_graph_info() {
      (0,0,0) => {break;}
      (nodes, edges, blues) => {
        let mut graph = read_edges_info(nodes,edges);
        let max_red = graph.kruskal(Color::Red);
        let max_blue = graph.kruskal(Color::Blue);
        println! ( "{}", if blues < nodes-max_red-1 || blues > max_blue {0} else {1} );
      }
    }
  }
}



