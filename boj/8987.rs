use std::io;

struct Line {
    depth:    usize,
    interval: (usize, usize),
}
struct OpenArea {
    ybound: usize,
    xbound: (usize, usize),
}

impl Line {
    fn calc_exclusive_area(&self, open_area: &OpenArea) -> u64 {
        ((self.depth - open_area.ybound) * (open_area.xbound.1 - open_area.xbound.0)) as u64
    }
}

impl OpenArea {
    fn split(&self, line: &Line) -> (OpenArea, OpenArea) {
        (OpenArea { ybound: line.depth,
             xbound: (self.xbound.0, line.interval.0), },
        OpenArea { ybound: line.depth,
             xbound: (line.interval.1, self.xbound.1), })
    }
}

struct ParentQueue(Vec<usize>);
impl ParentQueue {
    fn get_parent(&mut self, nodes: &Vec<Line>) -> Option<usize> {
        // let new_node = &nodes[nodes.len() - 1];
        let new_node = nodes.last().unwrap();
        while let Some(index) = self.0.pop() {
            if nodes[index].depth <= new_node.depth {
                self.0.push(index);
                self.0.push(nodes.len() - 1);
                return Some(index)
            }
        }
        self.0.push(nodes.len() - 1);
        None
    }
}

struct Tree {
    root:  Option<usize>,
    elems: Vec<Node>,
}
#[derive(Default)]
struct Node {
    left:  Option<usize>,
    right: Option<usize>,
}

impl Tree {
    fn set_root(&mut self) {
        let mut new_root_node: Node = Default::default();
        new_root_node.left = self.root;
        self.elems.push(new_root_node);
        self.root = Some(self.elems.len() - 1);
    }
    fn add_node(&mut self, parent: usize) {
        let mut new_node: Node = Default::default();
        if let Some(prev_child) = self.elems[parent].right {
            new_node.left = Some(prev_child);
        }
        self.elems.push(new_node);
        self.elems[parent].right = Some(self.elems.len() - 1);
    }
}

struct Aquarium {
    lines: Vec<Line>,
    tree:  Tree,
}

struct Solver {
    holes: usize,
}
impl Solver {
    fn new() -> Solver {
        let mut s = String::new();
        io::stdin().read_line(&mut s).unwrap();
        let arg: Vec<usize> = s.as_mut_str().split_whitespace()
                               .map(|s| s.parse().unwrap())
                               .collect();
        Solver { holes: arg[0] }
    }
    fn solve(&mut self, aquarium: &Aquarium) -> u64 {
        let mut result = self.inner_solve(aquarium);
        let mut leaf_stack: Vec<u64> = Vec::new();
        let mut pending_stack: Vec<(usize, u64)> = Vec::new();

        while let Some(mut cur) = result.pop() {
            while !pending_stack.is_empty() {
                let pending = pending_stack.pop().unwrap();
                if pending.0 > cur.0 {
                    if !pending_stack.is_empty() {
                        let next_pending = pending_stack.pop().unwrap();
                        if next_pending.0 > cur.0 {
                            if next_pending.1 < pending.1 {
                                leaf_stack.push(next_pending.1);
                                cur.1 += pending.1;
                            } else {
                                leaf_stack.push(pending.1);
                                cur.1 += next_pending.1;
                            }
                            continue
                        } else {
                            pending_stack.push(next_pending);
                        }
                    }
                    cur.1 += pending.1;
                } else {
                    pending_stack.push(pending);
                    break
                }
            }
            pending_stack.push(cur);
        }
        leaf_stack.push(pending_stack.pop().unwrap().1);
        leaf_stack.sort_by(|a, b| b.cmp(a));
        let mut sum = 0;
        self.holes = if self.holes > leaf_stack.len() {
            leaf_stack.len()
        } else {
            self.holes
        };
        for i in 0..self.holes {
            sum += leaf_stack[i];
        }
        sum
    }
    fn inner_solve(&mut self, aquarium: &Aquarium) -> Vec<(usize, u64)> {
        let mut stack: Vec<(usize, usize, OpenArea)> = Vec::new();
        let mut area_stack: Vec<(usize, u64)> = Vec::new();

        stack.push((0,
                   aquarium.tree.root.unwrap(),
                   OpenArea { ybound: 0,
                        xbound: (0, aquarium.lines.last().unwrap().interval.1), }));

        while let Some((level, index, area)) = stack.pop() {
            area_stack.push((level, aquarium.lines[index].calc_exclusive_area(&area)));
            let (left_bound, right_bound) = area.split(&aquarium.lines[index]);
            if let Some(left) = aquarium.tree.elems[index].left {
                stack.push((level + 1, left, left_bound));
            }
            if let Some(right) = aquarium.tree.elems[index].right {
                stack.push((level + 1, right, right_bound));
            }
        }
        area_stack
    }
}

impl Aquarium {
    fn new() -> Aquarium {
        let mut aquarium = Aquarium { lines: Vec::new(),
            tree:  Tree { root:  None,
                elems: Vec::new(), }, };
        let mut parent_queue = ParentQueue(Vec::new());

        let mut s = String::new();
        io::stdin().read_line(&mut s).unwrap();
        let arg: Vec<usize> = s.as_mut_str().split_whitespace()
                               .map(|s| s.parse().unwrap())
                               .collect();
        io::stdin().read_line(&mut s).unwrap();

        for _i in 0..arg[0] / 2 - 1 {
            s.clear();
            io::stdin().read_line(&mut s).unwrap();
            let arg1: Vec<usize> = s.as_mut_str().split_whitespace()
                                    .map(|s| s.parse().unwrap())
                                    .collect();
            s.clear();
            io::stdin().read_line(&mut s).unwrap();
            let arg2: Vec<usize> = s.as_mut_str().split_whitespace()
                                    .map(|s| s.parse().unwrap())
                                    .collect();

            let new_line = Line { depth:    arg1[1],
                interval: (arg1[0], arg2[0]), };
            aquarium.lines.push(new_line);

            match parent_queue.get_parent(&aquarium.lines) {
                Some(parent) => aquarium.tree.add_node(parent),
                None => aquarium.tree.set_root(),
            }
        }
        io::stdin().read_line(&mut s).unwrap();
        aquarium
    }
}

fn main() {
    let aquarium = Aquarium::new();
    let mut solver = Solver::new();
    println!("{}", solver.solve(&aquarium));
}
