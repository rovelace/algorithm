// 소풍
// 시간 제한 	메모리 제한 	제출 	정답 	맞은 사람 	정답 비율
// 2 초 	128 MB 	448 	84 	53 	21.545%
// 문제

// 원장선생님께서는 K(1 ≤ K ≤ 62)명에게 소풍을 보내려 한다. 원장선생님께서는 1부터 N까지 번호가 붙은 N(K ≤ N ≤ 900)명의 학생들 중에서 K명의 학생들을 소풍에 보내려고 한다. 그런데 원장선생님께서는 중간에 싸움이 일어나면 안되므로 소풍을 갈 학생들이 모두 서로 친구 사이이기를 원한다. 원장선생님께서는 이러한 일을 이번에 조교로 참가한 고은이에게 친구 관계에 대한 정보를 F(1 ≤ F ≤ 5,600)개를 주시며 K명을 선발하라고 부탁하였다.

// 고은 조교를 도와 소풍을 가게 될 K명의 학생들을 결정하시오.
// 입력

// 첫째 줄에 공백으로 분리된 세 정수 K, N, F가 주어진다. 다음 F개의 줄에는 서로 친구 관계인 두 사람의 번호가 주어진다. 친구 관계는 상호적인 관계이므로 2번 학생이 4번 학생을 좋아하면 4번 학생도 2번 학생을 좋아한다.
// 출력

// 만약 K명의 친구 관계인 학생들이 존재하지 않는다면 -1을 출력한다. 그 외의 경우에는, K개의 줄에 학생들의 번호를 증가하는 순서로 한 줄에 한 개씩 출력한다. 여러 경우가 존재한다면 첫 번째 학생의 번호가 제일 작은 것을 출력한다. 첫 번째 학생의 번호가 같은 경우라면, 두 번째 학생의 번호가 작은 경우를 출력하고, 이와 같은 식으로 출력한다.

use std::io;

trait SortedSet {
    fn is_superset(&self, subset : &Self) -> bool;
}

impl<T> SortedSet for Vec<T> where T : Ord {
    fn is_superset(&self, subset: &Self) -> bool{
        let mut self_cur = 0;
        let mut subset_cur = 0;
        while self_cur < self.len() && subset_cur < subset.len() {
            if self[self_cur] < subset[subset_cur] {
                self_cur +=1;
            } else if self[self_cur] == subset[subset_cur]{
                self_cur+=1;
                subset_cur+=1;
            } else {
                break;
            }
        }
        subset_cur == subset.len()
    }
}

struct CompleteDFS {
    visit : Vec<bool>,
    backtrace : Vec<usize>,
    limit : usize,
}

impl CompleteDFS {
    fn new (nodes: usize, limit: usize) -> CompleteDFS {
        CompleteDFS{
            visit: vec![ false; nodes],
            backtrace : Vec::new(),
            limit : limit
        }
    }
    fn rollback(&mut self) {
        if let Some(i) = self.backtrace.pop() {
            self.visit[i] = false;
        }
    }
    fn visit(&mut self, graph: &Vec<Vec<usize>>,node: usize) -> bool {
        if self.visit[node] {
            false
        } else if graph[node].is_superset(&self.backtrace) {
            self.backtrace.push(node);
            self.visit[node] = true;
            true
        } else {
            false
        }
    }
    fn complete_dfs (&mut self, graph : &Vec<Vec<usize>>) -> Option<Vec<usize>> {
        for i in 0..graph.len() {
            self.visit(graph,i);
            self.inner_dfs(graph);
            if self.is_finished() {
                return Some(self.backtrace.clone());
            } else if self.backtrace.len() == 1{
                self.rollback();
            }
            else {
                panic!("DFS structure is not rollbacked!");
            }
        }
        None
    }
    fn is_finished (&self) -> bool {
        self.backtrace.len() == self.limit
    }
    fn inner_dfs (&mut self, graph : &Vec<Vec<usize>>) {
        let last = self.backtrace[self.backtrace.len()-1];
        if self.is_finished() { return; }
        for node in graph[last].clone() {
            if self.visit(graph,node) {
                self.inner_dfs(graph);
                if self.is_finished() { return; }
                self.rollback();
            }
        }
    }
}



fn main() {
    let mut s=  String::new();
    io::stdin().read_line(&mut s).unwrap();
    let arg : Vec<usize> = s.as_mut_str().split_whitespace().map(|s| s.parse().unwrap()).collect();
    let mut graph : Vec<Vec<usize>> =  vec![Vec::new(); arg[1]];
    for _i in 0..arg[2] {
        let mut s = String::new();
        io::stdin().read_line(&mut s).unwrap();
        let edge : Vec<usize> = s.as_mut_str().split_whitespace().map(|s| s.parse().unwrap()).map(|i: usize| i-1).collect();
        graph[edge[0]].push(edge[1]);
        graph[edge[1]].push(edge[0]);
    }
    for edges in &mut graph {
        edges.sort();
    }
    let mut dfs = CompleteDFS::new(arg[1], arg[0]);
    match dfs.complete_dfs(&graph) {
        Some(result)=> {
            for i in result {
                println!("{}",i+1);
            }
        }, 
        None => {println!("-1");}
    }

}